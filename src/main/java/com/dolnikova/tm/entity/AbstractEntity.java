package com.dolnikova.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractEntity {

    @Nullable
    private String id;
    @Nullable
    private String userId;
    @Nullable
    private String name;

}
