package com.dolnikova.tm.entity;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Task extends AbstractEntity {

    @Nullable
    private String id;
    @Nullable
    private String projectId;
    @Nullable
    private String userId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date startDate;
    @Nullable
    private Date endDate;

    public Task() {}

    public Task(@Nullable final String projectId, @Nullable final String name) {
        this.name = name;
        id = UUID.randomUUID().toString();
        this.projectId = projectId;
    }

}
