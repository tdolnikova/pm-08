package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.util.HashUtil;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class User extends AbstractEntity {

    @Nullable
    private String id;
    @Nullable
    private String userId;
    @Nullable
    private String login;
    @Nullable
    private String password;
    @Nullable
    private Role role;

    public User(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        this.id = UUID.randomUUID().toString();
        userId = this.id;
        this.login = login;
        this.password = HashUtil.stringToHashString(password);
        this.role = role;
    }

}
