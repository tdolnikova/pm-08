package com.dolnikova.tm.entity;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class Project extends AbstractEntity {

    @Nullable
    private String id;
    @Nullable
    private String userId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date startDate;
    @Nullable
    private Date endDate;

    public Project() {
    }

    public Project(@Nullable final String userId, @Nullable final String projectName) {
        this.name = projectName;
        this.userId = userId;
        id = UUID.randomUUID().toString();
    }


}
