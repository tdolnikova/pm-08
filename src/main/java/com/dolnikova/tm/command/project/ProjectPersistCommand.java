package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import lombok.NonNull;

public class ProjectPersistCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.PERSIST_PROJECT;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.PERSIST_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.INSERT_NEW_PROJECT_NAME);
        final String projectName = Bootstrap.scanner.nextLine();
        if (projectName.isEmpty()) return;
        final Project project = serviceLocator.getProjectService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
        if (project != null) {
            System.out.println(Constant.PROJECT_NAME_ALREADY_EXIST);
            return;
        }
        final Project newProject = new Project(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
        serviceLocator.getProjectService().persist(serviceLocator.getUserService().getCurrentUser().getId(), newProject);
        System.out.println(Constant.PROJECT + " " + projectName + " " + Constant.CREATED_M);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
