package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import lombok.NonNull;

public class ProjectMergeCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.MERGE_PROJECT;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.MERGE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.INSERT_PROJECT_NAME);
        Project foundProject = null;
        while (foundProject == null) {
            final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            foundProject = serviceLocator.getProjectService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (foundProject == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + Constant.TRY_AGAIN);
            else {
                System.out.println(Constant.INSERT_NEW_PROJECT_NAME);
                boolean newNameInserted = false;
                while (!newNameInserted) {
                    final String newProjectName = Bootstrap.scanner.nextLine();
                    if (newProjectName.isEmpty()) return;
                    serviceLocator.getProjectService().merge(serviceLocator.getUserService().getCurrentUser().getId(), newProjectName, foundProject);
                    System.out.println(Constant.PROJECT_UPDATED);
                    newNameInserted = true;
                }
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
