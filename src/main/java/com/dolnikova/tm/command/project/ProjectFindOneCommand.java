package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import lombok.NonNull;

public class ProjectFindOneCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.FIND_PROJECT;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.FIND_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return;
        }
        System.out.println(Constant.INSERT_PROJECT_NAME);
        Project project = null;
        while (project == null) {
            final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
            else System.out.println(Constant.PROJECT_NAME + project.getName());
        }
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
