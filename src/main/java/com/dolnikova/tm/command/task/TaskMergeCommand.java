package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

public class TaskMergeCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.MERGE_TASK;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.MERGE_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        Project project = findProject();
        if (project == null) return;
        List<Task> projectTasks = serviceLocator.getTaskService().getTasksByProjectId(serviceLocator.getUserService().getCurrentUser().getId(), project.getId());
        int taskSize = projectTasks.size();
        if (taskSize == 0) {
            System.out.println(Constant.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(Constant.IN_PROJECT + project.getName() + " " + taskSize + " " + Constant.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Constant.INSERT_TASK_ID);
        boolean taskUpdated = false;
        while (!taskUpdated) {
            String taskIdToUpdate = Bootstrap.scanner.nextLine();
            if (taskIdToUpdate.isEmpty()) break;
            Task taskToUpdate = serviceLocator.getTaskService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), taskIdToUpdate);
            System.out.println(Constant.INSERT_NEW_TASK);
            String newTaskText = Bootstrap.scanner.nextLine();
            if (newTaskText.isEmpty()) break;
            //Task taskToAdd = new Task(project.getId(), newTaskText);
            serviceLocator.getTaskService().merge(serviceLocator.getUserService().getCurrentUser().getId(), newTaskText, taskToUpdate);
            System.out.println(Constant.TASK_UPDATED);
            taskUpdated = true;
        }
    }

    @Nullable
    public Project findProject() {
        if (serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return null;
        }
        if (serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        Project project = null;
        while (project == null) {
            final String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectService().findOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
