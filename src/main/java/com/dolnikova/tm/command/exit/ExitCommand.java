package com.dolnikova.tm.command.exit;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import lombok.NonNull;

public class ExitCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.EXIT;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.EXIT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.exit(0);
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
