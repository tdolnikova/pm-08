package com.dolnikova.tm.command.about;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.jcabi.manifests.Manifests;
import lombok.NonNull;

public class AboutCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.ABOUT;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.ABOUT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task Manager ver.SE-07, " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));

    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
