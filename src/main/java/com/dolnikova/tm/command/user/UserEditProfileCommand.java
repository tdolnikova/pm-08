package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.enumeration.DataType;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

public class UserEditProfileCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.USER_EDIT_PROFILE;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.USER_EDIT_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.ENTER_FIELD_NAME + "\n" + DataType.LOGIN + "\n" + DataType.ROLE);

        String fieldToEdit = "";
        while (fieldToEdit.isEmpty()) {
            fieldToEdit = Bootstrap.scanner.nextLine();
        }
        if (fieldToEdit.equals(DataType.LOGIN.toString())) {
            changeData(DataType.LOGIN);
            System.out.println(Constant.LOGIN_CHANGED);
        }
        if (fieldToEdit.equals(DataType.ROLE.toString())) {
            changeData(DataType.ROLE);
            System.out.println(Constant.ROLE_CHANGED);
        }

    }

    public void changeData(@Nullable DataType dataType) {
        System.out.println(Constant.ENTER + dataType.toString());
        String newData = "";
        while (newData.isEmpty()) {
            newData = Bootstrap.scanner.nextLine();
        }
        serviceLocator.getUserService().merge(newData, serviceLocator.getUserService().getCurrentUser().getId(), serviceLocator.getUserService().getCurrentUser(), dataType);
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserService().getCurrentUser() == null));
    }
}
