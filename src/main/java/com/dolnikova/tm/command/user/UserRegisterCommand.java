package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.util.HashUtil;
import lombok.NonNull;

import java.util.UUID;

public class UserRegisterCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.USER_REG;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.USER_REG_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[REGISTRATION]");
        System.out.println(Constant.REG_ENTER_LOGIN);
        String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(Constant.REG_ENTER_PASSWORD);
        String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }
        if (serviceLocator.getUserService().findOne(login) == null) {
            final User newUser = new User(login, password, Role.USER);
            serviceLocator.getUserService().persist(newUser.getId(), newUser);
            System.out.println(Constant.REG_USER_REGISTERED);
        } else {
            System.out.println(Constant.REG_USER_EXISTS);
        }

    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUserService().getCurrentUser() == null;
    }
}
