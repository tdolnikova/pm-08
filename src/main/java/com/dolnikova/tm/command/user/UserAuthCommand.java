package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.User;
import lombok.NonNull;

public class UserAuthCommand extends AbstractCommand {

    @NonNull
    @Override
    public String command() {
        return Constant.USER_AUTH;
    }

    @NonNull
    @Override
    public String description() {
        return Constant.USER_AUTH_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[AUTHORIZATION]");
        System.out.println(Constant.REG_ENTER_LOGIN);
        String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(Constant.REG_ENTER_PASSWORD);
        String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }

        // добавить проверку пароля

        final User user = serviceLocator.getUserService().findOne(login);
        if (user == null) {
            System.out.println(Constant.REG_USER_NOT_FOUND);
            return;
        }
        serviceLocator.getUserService().setCurrentUser(user);
        System.out.println(Constant.REG_USER_SUCCESSFUL_LOGIN);
    }

    @Override
    public boolean isSecure() {
        return serviceLocator.getUserService().getCurrentUser() == null;
    }
}
