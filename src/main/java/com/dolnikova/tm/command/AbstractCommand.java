package com.dolnikova.tm.command;

import com.dolnikova.tm.api.bootstrap.ServiceLocator;
import lombok.NonNull;

public abstract class AbstractCommand {

    @NonNull public ServiceLocator serviceLocator;
    public void setServiceLocator(@NonNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @NonNull public abstract String command();
    @NonNull public abstract String description();
    public abstract void execute() throws Exception;
    public abstract boolean isSecure();
}