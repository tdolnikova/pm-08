package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    @Nullable
    @Override
    public List<Task> getTasksByProjectId(@NonNull final String userId, @NonNull final String projectId) {
        final List<Task> allTasks = findAll(userId);
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task : allTasks) {
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

}
