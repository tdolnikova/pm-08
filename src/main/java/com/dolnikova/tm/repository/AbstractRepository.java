package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

import java.util.*;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final Map<String, E> map = new LinkedHashMap<>();

    public AbstractRepository() {
    }

    @Nullable
    @Override
    public List<E> findAll(@NonNull final String userId) {
        final List<E> all = new ArrayList<>();
        for (Map.Entry<String, E> entity : map.entrySet()) {
            if (entity.getValue().getUserId().equals(userId)) all.add(entity.getValue());
        }
        return all;
    }

    @Nullable
    @Override
    public E findOne(@NonNull final String userId, @NonNull final String id) {
        for (Map.Entry<String, E> entity : map.entrySet()) {
            final E foundEntity = entity.getValue();
            if (id.equals(foundEntity.getId())
                    && entity.getValue().getUserId().equals(userId))
                return foundEntity;
        }
        return null;
    }

    @NonNull
    public E findOne(@NonNull final String login) {
        return null;
    }

    @Override
    public void persist(@NonNull final String userId, @NonNull final E entity) {
        if (entity.getUserId().equals(userId)) {
            map.put(entity.getId(), entity);
        }
    }

    @Override
    public void merge(@NonNull final String userId, @NonNull final String name, @NonNull final E entity) {
        if (entity.getUserId().equals(userId)) {
            entity.setName(name);
        }
    }

    public void merge(@NonNull final String newData, @NonNull final String userId, @NonNull final User user, @NonNull final DataType dataType) {

    }

    @Override
    public void remove(@NonNull final String userId, @NonNull final String id) {
        final E entityToRemove = map.get(id);
        if (entityToRemove.getUserId().equals(userId)) map.remove(id);
    }

    @Override
    public void remove(@NonNull final String userId, @NonNull final E entity) {
        if (entity.getUserId().equals(userId)) map.remove(entity.getId());
    }

    @Override
    public void removeAll(@NonNull final String userId) {
        for (Iterator<Map.Entry<String, E>> it = map.entrySet().iterator(); it.hasNext(); ) {
            final Map.Entry<String, E> entry = it.next();
            if (entry.getValue().getUserId().equals(userId)) {
                it.remove();
            }
        }
    }

    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return null;
    }
}
