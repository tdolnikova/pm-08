package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.util.HashUtil;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository() {
    }

    @Nullable
    @Override
    public User findOne(@NonNull final String login) {
        for (Map.Entry<String, User> user : map.entrySet()) {
            if (user.getValue().getLogin().equals(login)) return user.getValue();
        }
        return null;
    }

    @Override
    public void merge(@NonNull final String newData, @NonNull final String userId, @NonNull final User user, @NonNull final DataType dataType) {
        switch (dataType) {
            case PASSWORD:
                if (user.getUserId().equals(userId)) {
                    user.setPassword(HashUtil.stringToHashString(newData));
                    map.put(user.getUserId(), user);
                }
                break;
            case LOGIN:
                user.setLogin(newData);
                break;
            case ROLE:
                if (Role.USER.toString().equalsIgnoreCase(newData)) user.setRole(Role.USER);
                if (Role.ADMIN.toString().equalsIgnoreCase(newData)) user.setRole(Role.ADMIN);
                break;
        }
        map.put(user.getUserId(), user);
    }

    @Override
    public void removeAll(@NonNull String userId) {
        map.clear();
    }

}
