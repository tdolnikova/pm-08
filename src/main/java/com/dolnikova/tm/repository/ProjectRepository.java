package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
    }

}
