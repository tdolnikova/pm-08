package com.dolnikova.tm.api.bootstrap;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.command.AbstractCommand;
import lombok.Getter;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

public interface ServiceLocator {
    @NonNull IProjectService getProjectService();
    @NonNull ITaskService getTaskService();
    @NonNull IUserService getUserService();
    @NonNull Collection<AbstractCommand> getCommands();
}
