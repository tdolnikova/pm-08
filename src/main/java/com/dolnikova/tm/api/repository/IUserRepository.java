package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Override
    List<User> findAll(@Nullable final String userId);

    @Nullable
    @Override
    User findOne(@Nullable final String userId, @Nullable final String id);

    @Nullable
    User findOne(@Nullable final String login);

    @Override
    void persist(@Nullable final String userId, @Nullable final User entity);

    @Override
    void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final User entity);

    void merge(@Nullable final String newData, @Nullable final String userId, @Nullable final User user, @Nullable final DataType dataType);

    @Override
    void remove(@Nullable final String userId, @Nullable final String id);

    @Override
    void remove(@Nullable final String userId, @Nullable final User entity);

    @Override
    void removeAll(@Nullable final String userId);

}
