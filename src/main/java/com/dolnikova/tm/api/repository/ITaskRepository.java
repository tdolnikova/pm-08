package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    @Override
    List<Task> findAll(@Nullable final String userId);

    @Nullable
    @Override
    Task findOne(@Nullable final String userId, @Nullable final String id);

    @Override
    void persist(@Nullable final String userId, @Nullable final Task entity);

    @Override
    void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final Task entity);

    @Override
    void remove(@Nullable final String userId, @Nullable final String id);

    @Override
    void remove(@Nullable final String userId, @Nullable final Task entity);

    @Override
    void removeAll(@Nullable final String userId);

    @Nullable
    List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId);
}
