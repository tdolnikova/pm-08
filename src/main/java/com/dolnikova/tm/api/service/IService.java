package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.AbstractEntity;
import org.jetbrains.annotations.Nullable;


import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    List<E> findAll(@Nullable final String userId);

    @Nullable
    E findOne(@Nullable final String userId, @Nullable final String id);

    void persist(@Nullable final String userId, @Nullable final E entity);

    void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final E entityToMerge);

    void remove(@Nullable final String userId, @Nullable final E entity);

    void removeAll(@Nullable final String userId);

}
