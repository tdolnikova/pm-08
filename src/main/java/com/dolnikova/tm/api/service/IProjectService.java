package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Project;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    @Override
    List<Project> findAll(@Nullable final String userId);

    @Nullable
    @Override
    Project findOne(@Nullable final String userId, @Nullable final String id);

    @Override
    void persist(@Nullable final String userId, @Nullable final Project entity);

    @Override
    void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final Project entityToMerge);

    @Override
    void remove(@Nullable final String userId, @Nullable final Project entity);

    @Override
    void removeAll(@Nullable final String userId);
}
