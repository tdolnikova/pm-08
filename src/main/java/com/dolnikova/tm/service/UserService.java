package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    private IUserRepository userRepository;
    private User currentUser = null;

    public UserService() {
    }

    public UserService(@NonNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public List<User> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) return null;
        return userRepository.findAll(userId);
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty() || userRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return userRepository.findOne(userId, id);
    }

    @Override
    public void persist(@Nullable final String userId, @Nullable final User entity) {
        if (userId.isEmpty() || entity == null) return;
        userRepository.persist(userId, entity);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final User entity) {
        if (userId.isEmpty() || entity == null) return;
        userRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        userRepository.removeAll(userId);
    }

    @Override
    public User findOne(@Nullable final String login) {
        if (login.isEmpty()) return null;
        return userRepository.findOne(login);
    }

    @Override
    public void merge(@Nullable final String newPassword, @Nullable final String userId, @Nullable final User user) {
        if (newPassword.isEmpty() || userId.isEmpty() || user == null) return;
        userRepository.merge(newPassword, userId, user);
    }

    @Override
    public void merge(@Nullable final String newData, @Nullable final String userId, @Nullable final User user, @Nullable final DataType dataType) {
        if (newData.isEmpty() || userId.isEmpty() || user == null || dataType == null) return;
        userRepository.merge(newData, userId, user, dataType);
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@NonNull final User currentUser) {
        this.currentUser = currentUser;
    }
}
