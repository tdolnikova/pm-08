package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private IProjectRepository projectRepository;
    public ProjectService() {
    }

    public ProjectService(@NonNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty() || projectRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return projectRepository.findOne(userId, id);
    }

    @Override
    public void persist(@Nullable final String userId, @Nullable final Project entity) {
        if (userId.isEmpty() || entity == null) return;
        projectRepository.persist(userId, entity);
    }

    @Override
    public void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final Project entityToMerge) {
        if (userId.isEmpty() || newName.isEmpty() || entityToMerge == null) return;
        projectRepository.merge(userId, newName, entityToMerge);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Project entity) {
        if (userId.isEmpty() || entity == null) return;
        projectRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        projectRepository.removeAll(userId);
    }

}
