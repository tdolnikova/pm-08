package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.IRepository;
import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public abstract class AbstractService <E extends AbstractEntity> implements IService<E> {

    private IRepository<E> abstractRepository;

    AbstractService() {
    }

    AbstractService(@NonNull final IRepository<E> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) return null;
        return findAll(userId);
    }

    @Nullable
    @Override
    public E findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty() || abstractRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return abstractRepository.findOne(userId, id);
    }

    @Nullable
    public E findOne(@Nullable final String login) {
        return null;
    }

    @Override
    public void persist(@Nullable final String userId, @Nullable final E entity) {
        if (userId.isEmpty() || entity == null) return;
        abstractRepository.persist(userId, entity);
    }

    @Override
    public void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final E entityToMerge) {
        if (userId.isEmpty() || newName.isEmpty() || entityToMerge == null) return;
        abstractRepository.merge(userId, newName, entityToMerge);
    }

    public void merge(@Nullable final String newData, @Nullable final String userId, @Nullable final User user, @Nullable final DataType dataType) {

    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final E entity) {
        if (userId.isEmpty() || entity == null) return;
        abstractRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        abstractRepository.removeAll(userId);
    }

}
