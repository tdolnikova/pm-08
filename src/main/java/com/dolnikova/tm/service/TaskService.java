package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.Nullable;
import lombok.NonNull;

import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService() {
    }

    public TaskService(@NonNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) return null;
        return taskRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId.isEmpty() || taskRepository.findAll(userId).isEmpty() || id.isEmpty()) return null;
        return taskRepository.findOne(userId, id);
    }

    @Nullable
    public Task findOne(@Nullable final String login) {
        return null;
    }

    @Override
    public void persist(@Nullable final String userId, @Nullable final Task entity) {
        if (userId.isEmpty() || entity == null) return;
        taskRepository.persist(userId, entity);
    }

    @Override
    public void merge(@Nullable final String userId, @Nullable final String newName, @Nullable final Task entityToMerge) {
        if (userId.isEmpty() || newName.isEmpty() || entityToMerge == null) return;
        taskRepository.merge(userId, newName, entityToMerge);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task entity) {
        if (userId.isEmpty() || entity == null) return;
        taskRepository.remove(userId, entity);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    @Nullable
    @Override
    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return null;
        return taskRepository.getTasksByProjectId(userId, projectId);
    }

}
