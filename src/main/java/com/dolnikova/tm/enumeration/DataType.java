package com.dolnikova.tm.enumeration;

public enum DataType {
    LOGIN,
    PASSWORD,
    ROLE
}
