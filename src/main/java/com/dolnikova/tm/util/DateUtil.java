package com.dolnikova.tm.util;

import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    private static final String DATE_FORMAT = "DD.MM.YYYY";
    private static SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

    @Nullable
    public String dateToString(@Nullable final Date date){
        return dateFormat.format(date);
    }

    @Nullable
    public Date stringToDate(@Nullable final String dateString){
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
