package com.dolnikova.tm.util;

import org.jetbrains.annotations.Nullable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public class HashUtil {

    @Nullable
    public static String stringToHashString(@Nullable final String string) {
        String hashString = "";
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte[] bytes = md.digest();
            hashString = DatatypeConverter.printHexBinary(bytes);
        } catch (Exception e) {e.printStackTrace();}
        return hashString;
    }

}
